package com.relations.m2m.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.relations.m2m.model.Subjects;

@Repository
public interface SubjectRepository extends JpaRepository<Subjects, Integer> {

}
