package com.relations.m2m.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.relations.m2m.model.Subjects;
import com.relations.m2m.repository.SubjectRepository;

@Service
public class SubTeacherService {

	@Autowired
	private SubjectRepository subjectRepository;

	public List<Subjects> getAllSub() {
		return subjectRepository.findAll();
	}

	public Subjects saveSub(Subjects subjects) {
		return subjectRepository.save(subjects);

	}

	public Subjects updateSubject(Subjects subject) {
		return subjectRepository.saveAndFlush(subject);
	}

}
