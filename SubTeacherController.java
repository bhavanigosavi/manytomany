package com.relations.m2m.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.relations.m2m.model.Subjects;
import com.relations.m2m.service.SubTeacherService;

@RestController
@Component
public class SubTeacherController {

	@Autowired
	private SubTeacherService subTeacherService;

	@GetMapping(value = "/subjects1", produces = { MediaType.APPLICATION_JSON_VALUE })

	public ResponseEntity<List<Subjects>> getSubjects() {
		List<Subjects> subjects = subTeacherService.getAllSub();
		return ResponseEntity.status(200).body(subjects);
	}

	@PostMapping(value = "subjects2")
	public ResponseEntity<Subjects> saveSub(@RequestBody Subjects subjects) {
		return ResponseEntity.status(200).body(subTeacherService.saveSub(subjects));
	}

	@PutMapping(value = "subjects/{id}")
	public ResponseEntity<Subjects> updateSubject(@RequestBody Subjects subject, @PathVariable("id") int id) {
		subject.setId(id);

		return ResponseEntity.status(201).body(subject);
	}

}
